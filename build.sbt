organization := "mobi.efarmer"

name := "push-server"

version := "0.7"

scalaVersion := "2.11.7"

libraryDependencies ++= {
  val akkaVer = "2.3.9"
  val sprayVer = "1.3.3"
  val slickVer = "3.0.1"

  Seq(
    "io.spray" %% "spray-servlet" % sprayVer,
    "io.spray" %% "spray-routing" % sprayVer,
    "io.spray" %% "spray-client" % sprayVer,
    "io.spray" %%  "spray-json" % "1.3.2",
    "com.typesafe.akka" %% "akka-actor" % akkaVer,
    "com.typesafe.slick" %% "slick" % slickVer,
    "com.typesafe" % "config" % "1.3.0",
    "com.zaxxer" % "HikariCP" % "2.3.9",
    "org.postgresql" % "postgresql" % "9.4-1200-jdbc41",
    "javax.servlet" % "javax.servlet-api" % "3.0.1" % "provided"
  )
}

dependencyOverrides += "org.scala-lang.modules" %% "scala-xml" % "1.0.4"

webappWebInfClasses := true

javaOptions in Tomcat ++= Seq(
  "-Xdebug",
  "-Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5005"
)

enablePlugins(TomcatPlugin)
