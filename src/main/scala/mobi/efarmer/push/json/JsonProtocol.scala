package mobi.efarmer.push.json

import mobi.efarmer.push.actor._
import mobi.efarmer.push.model.ClientInfo
import spray.httpx.SprayJsonSupport
import spray.json._

/**
 * @author Maxim Maximchuk
 *         date 13.08.2015.
 */
object JsonProtocol extends DefaultJsonProtocol with SprayJsonSupport {

  implicit def clientInfoFormat = jsonFormat2(ClientInfo.apply)

  implicit def pushNotificationFormat = jsonFormat3(PushNotification.apply)

  implicit def pushDataFormat = jsonFormat2(PushData.apply)

  // GCMApi
  implicit object SendNotificationFormat extends RootJsonFormat[SendNotification] {

    def read(json: JsValue) = ???

    def write(obj: SendNotification) = {
      JsObject("notification" -> JsObject(
        "title" -> JsString(obj.title),
        "text" -> JsString(obj.msg)
      ),
        "to" -> JsString(obj.key)
      )
    }
  }

  implicit object SendDataFormat extends RootJsonFormat[SendData] {
    def read(json: JsValue) = ???

    def write(obj: SendData) = {
      JsObject("data" -> obj.data, "to" -> JsString(obj.key))
    }

  }

  implicit object GCMResponseFormat extends RootJsonFormat[GCMResponse] {
    def read(value: JsValue) = {
      value.asJsObject.getFields("success", "failure") match {
        case Seq(JsNumber(success), JsNumber(failure)) =>
          GCMResponse(success == 1, failure == 1, value.toString())
      }
    }

    def write(obj: GCMResponse) = ???
  }

}
