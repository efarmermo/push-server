package mobi.efarmer.push.actor

import akka.actor.{Actor, Props}
import mobi.efarmer.push.model.ClientInfoDto
import spray.json.JsObject

import scala.concurrent.ExecutionContext.Implicits.global
import scala.language.postfixOps

/**
 * @author Maxim Maximchuk
 *         date 09.08.2015.
 */
case class PushNotification(userUri: String, title: String, msg: String)
case class PushData(userUri: String, data: JsObject)
class ClientActionsActor extends Actor {

  def receive = {
    case PushNotification(user, title, msg) =>
      val sender = context.actorOf(Props[GCMApiActor])
      ClientInfoDto.keys(user).map(_.map(SendNotification(_, title, msg)).foreach(sender !))
    case PushData(user, data) =>
      val sender = context.actorOf(Props[GCMApiActor])
      ClientInfoDto.keys(user).map(_.map(SendData(_, data)).foreach(sender !))
  }

}
