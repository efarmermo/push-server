package mobi.efarmer.push.actor

import akka.actor.Actor
import akka.event.Logging
import mobi.efarmer.push.config.GCMConfig
import mobi.efarmer.push.model.ClientInfoDto
import spray.client.pipelining._
import spray.http.HttpRequest
import spray.json._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

import mobi.efarmer.push.json.JsonProtocol._

/**
 * @author Maxim Maximchuk
 *         date 10.08.2015.
 */
case class SendNotification(key: String, title: String, msg: String)

case class SendData(key: String, data: JsObject)

case class GCMResponse(success: Boolean, failure: Boolean, content: String)

class GCMApiActor extends Actor {

  val log = Logging(context.system, this)

  val pipeline: HttpRequest => Future[GCMResponse] = (
    addHeader("Authorization", "key=" + GCMConfig.apiKey)
      ~> sendReceive
      ~> unmarshal[GCMResponse]
    )

  def receive = {
    case notification: SendNotification =>
      log.info(notification.toJson.toString())
      processResponse(notification.key) {
        pipeline {
          Post(GCMConfig.url, notification)
        }
      }
    case data: SendData =>
      log.info(data.toJson.toString())
      processResponse(data.key) {
        pipeline {
          Post(GCMConfig.url, data)
        }
      }
  }

  def processResponse(key: String)(response: => Future[GCMResponse]) = {
    response onComplete {
      case Success(resp) =>
        log.info(resp.toString)
        if (resp.failure) ClientInfoDto -= key
      case Failure(err) =>
        log.error(err, err.getMessage)
        ClientInfoDto -= key
    }
  }

}
