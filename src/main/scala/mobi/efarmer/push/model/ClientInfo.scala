package mobi.efarmer.push.model

import slick.driver.PostgresDriver.api._
import spray.httpx.SprayJsonSupport
import spray.json.DefaultJsonProtocol

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.util.{Failure, Success}

/**
 * @author Maxim Maximchuk
 *         date 09.08.2015.
 */
case class ClientInfo(clientKey: String, userUri: String)
class ClientInfoT(tag: Tag) extends Table[ClientInfo](tag, "client_info") {
  def clientKey = column[String]("client_key", O.PrimaryKey)
  def userUri = column[String]("user_uri")

  def * = (clientKey, userUri) <> (ClientInfo.tupled, ClientInfo.unapply)
}

object ClientInfoDto extends BaseDto {
  val query = TableQuery[ClientInfoT]

  def exist(clientKey: String) = {
    val action = query.filter(_.clientKey === clientKey).length.result
    val res = db.run(action)
    res.map(_ > 0)
  }

  def +=(clientInfo: ClientInfo) = {
    val action = DBIO.seq(
      query += clientInfo
    )
    db.run(action)
  }

  def -=(clientKey: String) = {
    val action = query.filter(_.clientKey === clientKey).delete
    db.run(action)
  }
  
  def keys(userUri: String) = {
    val action = query.filter(_.userUri === userUri).map(_.clientKey).result
    db.run(action)
  }
}


