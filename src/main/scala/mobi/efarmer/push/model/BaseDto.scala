package mobi.efarmer.push.model

import slick.jdbc.JdbcBackend._
import slick.lifted.TableQuery

/**
 * @author Maxim Maximchuk
 *         date 09.08.2015.
 */
trait BaseDto {
  val db = Database.forConfig("db")
}
