package mobi.efarmer.push.rest

import akka.actor.Props
import mobi.efarmer.push.actor._
import mobi.efarmer.push.config.GCMConfig
import mobi.efarmer.push.json.JsonProtocol._
import mobi.efarmer.push.model._
import spray.http.StatusCodes._
import spray.routing.HttpServiceActor

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Failure

/**
 * @author Maxim Maximchuk
 *         date 06.08.2015.
 */

class ApiService extends HttpServiceActor {

  def receive = runRoute {
    pathPrefix("api") {
      get {
        path("sender-id") {
          complete(GCMConfig.senderId)
        }
      } ~
        post {
          path("register") {
            entity(as[ClientInfo]) {
              clientInfo =>
                ctx => {
                  ClientInfoDto.exist(clientInfo.clientKey) onComplete {
                    case util.Success(exist) =>
                      if (!exist) {
                        val res = ClientInfoDto += clientInfo
                        res.onComplete {
                          case util.Success(_) => ctx.complete(OK)
                          case Failure(e) => ctx.failWith(e)
                        }
                      } else {
                        ctx.complete(Found, "already registered")
                      }
                    case Failure(e) => ctx.failWith(e)
                  }
                }
            }
          } ~
            path("send-notification") {
              entity(as[PushNotification]) {
                pushNotification => {
                  context.actorOf(Props[ClientActionsActor]) ! pushNotification
                  complete(OK)
                }
              }
            } ~
            path("send-data") {
              entity(as[PushData]) {
                pushData => {
                  context.actorOf(Props[ClientActionsActor]) ! pushData
                  complete(OK)
                }
              }
            }

        }
    }
  }

}
