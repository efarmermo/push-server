package mobi.efarmer.push.config

import com.typesafe.config.ConfigFactory

/**
 * @author Maxim Maximchuk
 *         date 09.08.2015.
 */
object AppConfig {

  private val config = ConfigFactory.load()

  val host = config.getString("server.hostname")
  val port = config.getInt("server.port")

}
