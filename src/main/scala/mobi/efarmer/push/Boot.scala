package mobi.efarmer.push

import akka.actor.{Props, ActorSystem}
import akka.io.IO
import akka.util.Timeout
import mobi.efarmer.push.config.AppConfig
import mobi.efarmer.push.rest.ApiService
import spray.can.Http
import spray.servlet.WebBoot
import scala.concurrent.duration._

/**
 * @author Maxim Maximchuk
 *         date 06.08.2015.
 */
class Boot extends WebBoot {

  val system = ActorSystem("push-server")

  val serviceActor = system.actorOf(Props[ApiService], "register-service")

}
